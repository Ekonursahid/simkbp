<?php

use Illuminate\Support\Facades\Input;
use App\BudgetingDetil;

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();

/*
|------------------------------------------------------------------------------------
| Admin
|------------------------------------------------------------------------------------
*/
Route::group(['prefix' => ADMIN, 'as' => ADMIN . '.', 'middleware'=>['auth', 'Role:"administrator"']], function () {
    Route::get('/', 'DashboardController@index')->name('dash');
    Route::resource('users', 'UserController');
    Route::resource('jabatan', 'JabatanController');
    Route::resource('rekening', 'RekeningController');
    Route::resource('menu', 'MenuController');
    Route::resource('karyawan', 'KaryawanController');
    Route::resource('cuti', 'CutiController');
    Route::resource('izin', 'IzinController');
    Route::resource('perjalananDinas', 'PerjalananDinasController');

    Route::resource('proyek', 'ProyekController');
    Route::get('proyek/print/{id}', 'ProyekController@print')->name('proyek.print');

    Route::resource('proyekDetil', 'ProyekDetilController');
    Route::get('proyek/{id}/proyekDetil/create', 'ProyekDetilController@create')->name('proyek.proyekDetil.create');
    Route::get('proyek/{idProyek}/proyekDetil/edit/{id}', 'ProyekDetilController@edit')->name('proyek.proyekDetil.edit');
    Route::put('proyek/{idProyek}/proyekDetil/{id}', 'ProyekDetilController@update')->name('proyek.proyekDetil.update');
    Route::post('proyek/{idProyek}/proyekDetil', 'ProyekDetilController@store')->name('proyek.proyekDetil.store');
    
    Route::resource('nonProyek', 'NonProyekController');
    Route::get('nonProyek/print/{id}', 'NonProyekController@print')->name('nonProyek.print');

    Route::resource('nonProyekDetil', 'NonProyekDetilController');
    Route::get('nonProyek/{id}/nonProyekDetil/create', 'NonProyekDetilController@create')->name('nonProyek.nonProyekDetil.create');
    Route::get('nonProyek/{idNonProyek}/nonProyekDetil/edit/{id}', 'NonProyekDetilController@edit')->name('nonProyek.nonProyekDetil.edit');
    Route::put('nonProyek/{idNonProyek}/nonProyekDetil/{id}', 'NonProyekDetilController@update')->name('nonProyek.nonProyekDetil.update');
    Route::post('nonProyek/{idNonProyek}/nonProyekDetil', 'NonProyekDetilController@store')->name('nonProyek.nonProyekDetil.store');

    Route::resource('uudp', 'UudpController');
    Route::get('uudp/print/{id}', 'UudpController@print')->name('uudp.print');
    
    Route::resource('uudpDetil', 'UudpDetilController');
    Route::get('uudp/{id}/UudpDetil/create', 'UudpDetilController@create')->name('uudp.uudpDetil.create');
    Route::get('uudp/{idUudp}/uudpDetil/edit/{id}', 'UudpDetilController@edit')->name('uudp.uudpDetil.edit');
    Route::put('uudp/{idUudp}/uudpDetil/{id}', 'UudpDetilController@update')->name('uudp.uudpDetil.update');
    Route::post('uudp/{idUudp}/uudpDetil', 'UudpDetilController@store')->name('uudp.uudpDetil.store');

    Route::resource('budgeting', 'BudgetingController');
    Route::get('budgetingDashboard', 'BudgetingController@dashboard')->name('budgeting.dashboard');
    Route::get('budgeting/print/{id}', 'BudgetingController@print')->name('budgeting.print');
    
    Route::resource('budgetingDetil', 'BudgetingDetilController');
    Route::get('budgeting/{id}/BudgetingDetil/create/personil', 'BudgetingDetilController@create')->name('budgeting.budgetingDetil.create.personil');
    Route::get('budgeting/{id}/BudgetingDetil/create/nonPersonil', 'BudgetingDetilController@create')->name('budgeting.budgetingDetil.create.nonPersonil');
    Route::get('budgeting/{idBudgeting}/budgetingDetil/edit/personil/{id}', 'BudgetingDetilController@edit')->name('budgeting.budgetingDetil.edit.personil');
    Route::get('budgeting/{idBudgeting}/budgetingDetil/edit/nonPersonil/{id}', 'BudgetingDetilController@edit')->name('budgeting.budgetingDetil.edit.nonPersonil');
    Route::put('budgeting/{idBudgeting}/budgetingDetil/{id}', 'BudgetingDetilController@update')->name('budgeting.budgetingDetil.update');
    Route::post('budgeting/{idBudgeting}/budgetingDetil', 'BudgetingDetilController@store')->name('budgeting.budgetingDetil.store');
    
});

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/ajax-subcat', function(){
    $cat_id = Input::get('cat_id');

    $subcategories = BudgetingDetil::where('budgeting_id', '=', $cat_id)->get();

    return Response::json($subcategories);
});
