<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">
				@if(auth()->user()->jabatan->kode_jabatan === 'J002')

				{!! Form::mySelect('karyawan_id', 'Karyawan', $karyawan, null, ['readonly'], ['class' => 'form-control select2']) !!}

				{!! Form::myTextArea('keperluan', 'Untuk Keperluan', ['readonly']) !!}
		
				{{-- {!! Form::myDate('date', 'tanggal_cuti', 'Tanggal Mulai Cuti', ['readonly']) !!} --}}

				{{-- {!! Form::myDate('date', 'tanggal_masuk', 'Tanggal Masuk Kembali', ['readonly']) !!} --}}

				{!! Form::myInput('number', 'jumlah_hari_ajuan', 'Jumlah hari cuti', ['readonly']) !!}
				
				<hr>

				{!! Form::mySelect('status_cuti', 'Status Cuti', config('variables.status_cuti'), null, ['class' => 'form-control select2']) !!}
		
				{!! Form::myInput('number', 'jumlah_hari_disetujui', 'Disetujui Selama') !!}

				{!! Form::myTextArea('alasan_tidak_disetujui', 'Tidak disetujui, dengan alasan') !!}

				{!! Form::mySelect('direksi', 'Atasan Langsung / Direksi', config('variables.atasan'), null, ['class' => 'form-control select2']) !!}

				{!! Form::mySelect('hrd', 'Kepala Urusan Sumber Daya Manusia', config('variables.atasan'), null, ['class' => 'form-control select2']) !!}

				@else

				{!! Form::mySelect('karyawan', 'Karyawan', $karyawan, null, ['class' => 'form-control select2']) !!}

				<button style="margin-bottom:10px;" name="submitbutton2" value="pilih" type="submit" class="btn btn-info">{{ "Pilih Karyawan" }}</button>

				{!! Form::hidden('karyawan_id', ($item == null) ? null : $item->id ) !!}

				{!! Form::myTextArea('alamat', 'Alamat', ['readonly'], ($item == null) ? null : $item->alamat ) !!}

				{!! Form::myInput('text', 'status', 'Status Karyawan', ['readonly'], ($item == null) ? null : $item->status ) !!}

				{!! Form::myInput('text', 'tanggal_masuk', 'Bekerja Sejak Tahun', ['readonly'], ($item == null) ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->tanggal_masuk)->year) !!}

				<hr>

				{!! Form::myTextArea('keperluan', 'Untuk Keperluan') !!}
		
				{!! Form::myDate('date', 'tanggal_cuti', 'Tanggal Mulai Cuti') !!}

				{!! Form::myDate('date', 'tanggal_masuk', 'Tanggal Masuk Kembali') !!}

				{!! Form::myInput('number', 'jumlah_hari_ajuan', 'Jumlah hari cuti') !!}
				
				@endif

		</div>  
	</div>
</div>