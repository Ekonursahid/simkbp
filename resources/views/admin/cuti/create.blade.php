@extends('admin.default')

@section('page-header')
	Cuti <small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
	{!! Form::open([
			'action' => ['CutiController@store'],
			'files' => true
		])
	!!}

		@include('admin.cuti.form')

		<button value="save" name="submitbutton2" type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
        <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
	{!! Form::close() !!}
	
@stop
