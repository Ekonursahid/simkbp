@extends('admin.default')

@section('page-header')
	Cuti <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['CutiController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.cuti.form')

		<button name="submitbutton2" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		
	{!! Form::close() !!}
	
@stop
