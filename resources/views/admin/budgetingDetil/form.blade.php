<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">
				{!! Form::hidden('budgeting_id', $idBudgeting) !!}
				{{-- {!! Form::mySelect('jenis_budget', 'Jenis Budget', config('variables.jenis_budget'), null, ['class' => 'form-control select2']) !!} --}}
				
				{!! Form::myInput('text', 'kode_rab', 'Kode RAB') !!}
				
				@if(Request::segment(6) == 'personil')	
					{!! Form::myInput('text', 'nama_personil', 'Nama Personil') !!}
					{!! Form::myInput('number', 'jumlah_bulan', 'Jumlah Bulan') !!}
					{!! Form::hidden('jenis_budget', '1') !!}

					@if(\Request::is('admin/budgeting/create'))
						{!! Form::myInput('textbox', 'honor_satuan_value_display', 'Jumlah Dana yang Diajukan (Rp)', ['id' => 'currencyField']) !!}
					@else
						{!! Form::myInput('textbox', 'honor_satuan', 'Honor Satuan (Rp)', ['id' => 'currencyField']) !!}
					@endif
					{!! Form::hidden('honor_satuan', null, ['id' => 'honor_satuan_value']) !!}

				@else 
					{!! Form::myInput('text', 'nama_kegiatan', 'Nama Kegiatan') !!}
					{!! Form::myInput('text', 'nama_unit', 'Nama Unit') !!}
					
					{!! Form::myInput('number', 'jumlah', 'Jumlah') !!}
					
					@if(\Request::is('admin/budgetingDetil/create'))
						{!! Form::myInput('textbox', 'harga_satuan_value_display', 'Jumlah Dana yang Diajukan (Rp)', ['id' => 'currencyField']) !!}
					@else
						{!! Form::myInput('textbox', 'harga_satuan', 'Harga Satuan (Rp)', ['id' => 'currencyField']) !!}
					@endif
					{!! Form::hidden('harga_satuan', null, ['id' => 'harga_satuan_value']) !!}
					
					
					{!! Form::hidden('jenis_budget', '0') !!}

					

				@endif
				
		</div>  
	</div>
</div>

@section('js')
<script>
document.getElementById("currencyField").onblur =function (){    

	//number-format the user input
	this.value = parseFloat(this.value.replace(/,/g, ""))
					.toFixed(2)
					.toString()
					.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

	//set the numeric value to a number input
	
	if(document.getElementById("harga_satuan_value") != null)
		document.getElementById("harga_satuan_value").value = this.value.replace(/,/g, "")

	if(document.getElementById("honor_satuan_value") != null)
	document.getElementById("honor_satuan_value").value = this.value.replace(/,/g, "")

}
</script>
	
@endsection