{{-- @if(auth()->user()->jabatan->kode_jabatan == 'J004') --}}
<div class="mB-20 mT-20">
    <a href="{{ route(ADMIN . '.budgeting.budgetingDetil.create.personil', $item->id) }}" class="btn btn-info">
        <i class="ti-plus"></i> {{ trans('app.add_button') }} Personil
    </a>
</div>
{{-- @endif --}}
<div class="bgc-white bd bdrs-3 p-20 mB-20">
    <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Nama Personil</th>
                <th>Jumlah Bulan</th>
                <th>Honor Satuan</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
        </thead>
        
        <tfoot>
            <tr>
                <th>Nama Personil</th>
                <th>Jumlah Bulan</th>
                <th>Honor Satuan</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
        </tfoot>
        
        <tbody>
            @foreach ($item->budgetingDetil as $itemDetil)
                @if($itemDetil->jenis_budget == '1')
                <tr>
                    <td><a href="{{ route(ADMIN . '.budgeting.budgetingDetil.edit.personil', ['idBudgeting' => $item->id, 'id' => $itemDetil->id]) }}">{{ $itemDetil->nama_personil }}</a></td>
                    <td>{{ number_format($itemDetil->jumlah_bulan, 0,",", "," ) }}</td>
                    <td>Rp {{ number_format($itemDetil->honor_satuan, 0,",", "," ) }}</td>
                    <td>Rp {{ number_format($itemDetil->total_personil, 0,",", "," ) }}</td>
                        
                    <td>
                        <ul class="list-inline">
                            @if(auth()->user()->jabatan->kode_jabatan === 'J005')
                            <li class="list-inline-item">
                                <a href="{{ route(ADMIN . '.budgeting.budgetingDetil.edit.personil', ['idBudgeting' => $item->id, 'id' => $itemDetil->id]) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm">Approval</span></a></li>
                            @else 
                            <li class="list-inline-item">
                                <a href="{{ route(ADMIN . '.budgeting.budgetingDetil.edit.personil', ['idBudgeting' => $item->id, 'id' => $itemDetil->id]) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>

                            
                            <li class="list-inline-item">
                                {!! Form::open([
                                    'class'=>'delete',
                                    'url'  => route(ADMIN . '.budgetingDetil.destroy', $itemDetil->id), 
                                    'method' => 'DELETE',
                                    ]) 
                                !!}

                                    <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i class="ti-trash"></i></button>
                                    
                                {!! Form::close() !!}
                            </li>
                            @endif
                            {{-- <li class="list-inline-item">
                                <a href="{{ route(ADMIN . '.budgetingDetil.show', $itemDetil->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-success btn-sm"><span class="ti-printer"></span></a></li> --}}
                            
                        </ul>
                    </td>
                </tr>
                @endif
            @endforeach
        </tbody>
    
    </table>
</div>
