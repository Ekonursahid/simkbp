@extends('admin.default')

@section('page-header')
	BudgetingDetil <small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
	{!! Form::open([
			'url' => route(ADMIN . '.budgeting.budgetingDetil.store', ['idBudgeting' => $idBudgeting]),
			'files' => true
		])
	!!}

		@include('admin.budgetingDetil.form')

		<button name="submitbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
		<a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>

	{!! Form::close() !!}
	
@stop
