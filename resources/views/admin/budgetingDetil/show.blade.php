@extends('admin.default')

@section('page-header')
	ProyekDetil <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['ProyekDetilController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.proyekDetil.form')

		{{-- <button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button> --}}
		
	{!! Form::close() !!}
	
	@include('admin.proyekDetilDetil.index')
	
@stop
