@extends('admin.default')

@section('page-header')
	BudgetingDetil <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'url' => route(ADMIN . '.budgeting.budgetingDetil.update', ['idBudgeting' => $idBudgeting, 'id' => $item->id]),
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.budgetingDetil.form')

		<button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		<a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
		
	{!! Form::close() !!}
	
@stop
