<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        table {
            border: 1px solid black;
            width: 100%;
            margin-top: 50px;
        }
        thead {
            text-align: center;
            /* border-top: 1px solid black; */
        }
        th {
            padding: 20px;
            /* border-top: 1px solid black; */
        }
        td {
            padding: 10px;
            border-top: 1px solid black;
        }
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th colspan="3">SURAT PERMOHONAN IZIN <br><br> PT. BHUMI PRASAJA</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3">Saya yang bertandatangan dibawah ini : </td>
            </tr>
            <tr>
                <td style="border: 0px;">
                    Nama : <b> {{$item->karyawan->nama_karyawan}} </b>
                </td>
                <td style="border: 0px;">Perusahaan / Dept : </td>
            </tr>
            <tr>
                <td style="border: 0px;">Alamat : </td>
                <td style="border: 0px;">Jabatan : <b>{{ $item->karyawan->jabaran }}</b></td>
            </tr>
            <tr>
                <td colspan="3" style="border: 0px;">Bermaksud mengajukan permohonan izin untuk tidak masuk bekerja, pada :</td>
            </tr>
            <tr>
                <td colspan="3" style="border: 0px;">
                    Hari / Tanggal : 
                    <b>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->tanggal_izin)->format('d-M-Y') }}</b>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border: 0px;">Alasan : <b>{{ $item->alasan }}</b></td>
            </tr>
            <tr>
                <td colspan="3" style="border: 0px;">
                    Demikian permohonan izin ini saya buat, atas perhatian dan kerjasamanya saya ucapkan terimakasih.
                </td>
            </tr>
            <tr>
                <td style="border: 0px;">Diajukan Oleh : </td>
                <td style="border: 0px;">Disetujui Oleh :</td>
                <td style="border: 0px;">Diketahui Oleh : </td>
            </tr>
            <tr>
                <td style="border: 0px;">Pemohon </td>
                <td style="border: 0px;">Atasan Langsung </td>
                <td style="border: 0px;">Kepala Bidang SDM</td>
            </tr>
            <tr>
                <td style="border: 0px; height:100px;">&nbsp; </td>
                <td style="border: 0px; height:100px;">&nbsp; </td>
                <td style="border: 0px; height:100px;">&nbsp; </td>
            </tr>
            <tr>
                <td style="border: 0px;"><b>{{$item->karyawan->nama_karyawan}}</b> </td>
                <td style="border: 0px;"> <b>{{$item->disetujui_oleh}}</b> </td>
                <td style="border: 0px;"><b>{{$item->diketahui_oleh}}</b></td>
            </tr>
        </tbody>
    </table>
</body>
</html>