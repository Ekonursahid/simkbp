<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">
				@if(auth()->user()->jabatan->kode_jabatan === 'J002')

				{!! Form::mySelect2('karyawan_id', 'Nama Karyawan', $karyawan, null, ['class' => 'form-control select2']) !!}

				{!! Form::myTextArea('alasan', 'Alasan') !!}

				<hr>

				{!! Form::mySelect('status_izin', 'Status Izin', config('variables.status_cuti'), null, ['class' => 'form-control select2']) !!}

				{!! Form::mySelect('disetujui_oleh', 'Disetujui Oleh', config('variables.atasan'), null, ['class' => 'form-control select2']) !!}

				{!! Form::mySelect('diketahui_oleh', 'Diketahui Oleh', config('variables.atasan'), null, ['class' => 'form-control select2']) !!}

				@else 

				{!! Form::mySelect2('karyawan_id', 'Nama Karyawan', $karyawan, null, ['class' => 'form-control select2']) !!}

				<button style="margin-bottom:10px;" name="submitbutton" value="pilih" type="submit" class="btn btn-info">{{ "Pilih Karyawan" }}</button>

				{!! Form::myTextArea('alamat', 'Alamat', ['readonly'], ($item == null) ? null : $item->alamat ) !!}

				{!! Form::myInput('text', 'perusahaan', 'Perusahaan / Dept', ['readonly'], ($item == null) ? null : $item->department ) !!}

				{!! Form::myInput('text', 'jabatan', 'Jabatan', ['readonly'], ($item == null) ? null : $item->jabatan ) !!}

				<hr>

				{!! Form::myDate('date', 'tanggal_izin', 'Hari/Tanggal') !!}

				{!! Form::myTextArea('alasan', 'Alasan') !!}

				@endif

		</div>  
	</div>
</div>