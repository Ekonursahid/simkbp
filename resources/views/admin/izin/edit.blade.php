@extends('admin.default')

@section('page-header')
	Izin <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['IzinController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.izin.form')

		<button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		
	{!! Form::close() !!}
	
@stop
