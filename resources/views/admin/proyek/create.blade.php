@extends('admin.default')

@section('page-header')
	Proyek <small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
	{!! Form::open([
			'action' => ['ProyekController@store'],
			'files' => true
		])
	!!}

		@include('admin.proyek.form')

		<button name="submitbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
        <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
		
	{!! Form::close() !!}
	
@stop
