@extends('admin.default')

@section('page-header')
	Proyek <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($itemProyek, [
			'action' => ['ProyekController@update', $itemProyek->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.proyek.form')

		{{-- <button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button> --}}
		
	{!! Form::close() !!}
	
	@include('admin.proyekDetil.index')
	
@stop
