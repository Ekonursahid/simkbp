<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        table {
            border: 1px solid black;
            width: 100%;
            margin-top: 50px;
        }
        thead {
            text-align: left;
            /* border-top: 1px solid black; */
        }
        th {
            padding: 20px;
            /* border-top: 1px solid black; */
        }
        td {
            padding: 10px;
            border-top: 1px solid black;
            border-right: 1px solid black;
            /* border-right: 1px solid black; */
        }
    </style>
</head>
<body>
    <div class="container">
        8. Form Pengajuan Dana Operasional Proyek
        <table>
            <thead>
                <tr>
                    <th colspan="7"> PT. BHUMI PRASAJA <br> BANDUNG</th>
                </tr>
                <tr>
                    <th colspan="7" style="text-align:right;"> No. : ........................................</th> 
                </tr>
                <tr>
                    <th colspan="7" style="text-align:right;"> Tanggal. : ........................................</th> 
                </tr>
                <tr>
                    <td colspan="7" style="text-align:center;"> <strong>FORM PENGAJUAN DANA OPERASIONAL PROYEK  </strong> </td> 
                </tr>
                <tr>
                    <td colspan="2"> Nama Pekerjaan : </td> 
                    <td colspan="5"> {{$item->nama_pekerjaan}} </td> 
                </tr>
                <tr>
                    <td colspan="2"> Kode Proyek : </td> 
                    <td colspan="5"> {{$item->kode_proyek}} </td> 
                </tr>
                <tr>
                    <td colspan="2"> Jumlah Dana yang diajukan : </td> 
                    <td colspan="5"> {{$item->jumlah_dana_ajukan}} </td> 
                </tr>
                <tr>
                    <td colspan="2"> Untuk Keperluan : </td> 
                    <td colspan="5"> {{$item->untuk_keperluan}} </td> 
                </tr>
                <tr>
                    <td colspan="2"> Kategori : </td> 
                    <td colspan="5"> {{$item->kategori}} </td> 
                </tr>
                <tr>
                    <td style="text-align:center;" colspan="7"><strong>RINCIAN PENGAJUAN</strong></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>No.</td>
                    <td>Kode RAB</td>
                    <td>Jenis Pengeluaran</td>
                    <td>Qty</td>
                    <td>Satuan</td>
                    <td>Harga Satuan</td>
                    <td>Jumlah (Rp)</td>
                </tr>
                @foreach ($item->proyekDetil as $itemDetil)
                <tr>
                    <td>{{ $itemDetil->id }}</td>
                    <td>{{ $itemDetil->kode_rab }}</td>
                    <td>{{ $itemDetil->jenis_pengeluaran }}</td>
                    <td>{{ $itemDetil->kuantitas }}</td>
                    <td>{{ $itemDetil->satuan }}</td>
                    <td>{{ number_format($itemDetil->harga_satuan, 0, ',', ',') }}</td>
                    <td>Rp {{ number_format($itemDetil->harga_satuan * $itemDetil->kuantitas, 0, ',', ',') }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
</body>
</html>