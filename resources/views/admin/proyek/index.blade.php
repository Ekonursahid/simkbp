@extends('admin.default')

@section('page-header')
    Proyek <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')
    {{-- @if(auth()->user()->jabatan->kode_jabatan == 'J004') --}}
    <div class="mB-20">
        <a href="{{ route(ADMIN . '.proyek.create') }}" class="btn btn-info">
            <i class="ti-plus"></i> {{ trans('app.add_button') }}
        </a>
    </div>
    {{-- @endif --}}

    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Kode Proyek</th>
                    <th>Nama Proyek</th>
                    <th>Tanggal Proyek</th>
                    <th style="text-align:right;">Jumlah Dana Pengajuan</th>
                    <th>Action</th>
                </tr>
            </thead>
            
            <tfoot>
                <tr>
                    <th>Kode Proyek</th>
                    <th>Nama Proyek</th>
                    <th>Tanggal Proyek</th>
                    <th style="text-align:right;">Jumlah Dana Pengajuan</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            
            <tbody>
                @foreach ($items as $item)
                    <tr>
                        <td><a href="{{ route(ADMIN . '.proyek.edit', $item->id) }}">{{ $item->kode_proyek }}</a></td>
                        <td>{{ $item->nama_pekerjaan }}</td>
                        <td>
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-M-Y') }} - 
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->diffForHumans(\Carbon\Carbon::now()) }}
                        </td>
                        <td style="text-align: right;">Rp {{ number_format($item->jumlah_dana_ajukan, 0, ',', ',') }}</td>
                        <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.proyek.show', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><i class="ti-pencil"></i></a></li>

                                @if(auth()->user()->jabatan->kode_jabatan === 'J004')
                                <li class="list-inline-item">
                                    {!! Form::open([
                                        'class'=>'delete',
                                        'url'  => route(ADMIN . '.proyek.destroy', $item->id), 
                                        'method' => 'DELETE',
                                        ]) 
                                    !!}

                                        <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i class="ti-trash"></i></button>
                                        
                                    {!! Form::close() !!}
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.proyek.print', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-success btn-sm"><span class="ti-printer"></span></a></li>
                                @endif
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        
        </table>
    </div>

@endsection