<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">

				{{-- {!! Form::mySelect('budgeting_detil_id', 'Budgeting Detil', $budget, null, ['class' => 'form-control select2']) !!}  --}}
				
				
				<div class="form-group">
					<label for="nama_pekerjaan">Budgeting</label>	
					<select name="budgeting_detil_id" class="form-control select2">
						@foreach ($budgeting as $data)
							<option disabled >{{ $data->nama_pekerjaan }}</option>
							@foreach ($data->BudgetingDetil as $data2)
								<option value="{{ $data2->id }}">{{ $data2->kode_rab }}</option>
							@endforeach
						@endforeach
					</select>
				</div>
				

				<button style="margin-bottom:10px;" name="submitbutton2" value="pilih" type="submit" class="btn btn-info">{{ "Pilih Budget" }}</button>

				{!! Form::myInput('text', 'kode_budget', 'Kode Budget', ['readonly'], ($item == null) ? null : $item->budgeting->kode_budget ) !!}

				{!! Form::myInput('text', 'nama_pekerjaan', 'Nama Pekerjaan', ['readonly'], ($item == null) ? null : $item->budgeting->nama_pekerjaan ) !!}
				
				{!! Form::myInput('text', 'jumlah_dana_ajukan', 'Jumlah Dana Pengajuan', ['readonly'], ($item == null) ? null : $item->budgeting->jumlah_dana_ajukan ) !!}

				{!! Form::myTextArea('untuk_keperluan', 'Untuk Keperluan', ['readonly'], ($item == null) ? null : $item->budgeting->untuk_keperluan ) !!}

				<hr>

				{!! Form::myInput('text', 'kode_rab', 'Kode RAB', ['readonly'], ($item == null) ? null : $item->kode_rab) !!}

				{!! Form::myInput('text', 'jenis_budget', 'Jenis Budget', ['readonly'], ($item == null) ? null : ($item->jenis_budget == 0) ? 'Non Personil' : 'Personil')!!}

				<hr>
				
				{!! Form::myInput('text', 'nama_pekerjaan', 'Nama Pekerjaan') !!}

				{!! Form::myInput('text', 'kode_proyek', 'Kode Proyek') !!}

				{!! Form::myInput('number', 'jumlah_dana_ajukan', 'Jumlah Dana yang Diajukan (Rp)') !!}

				{!! Form::myTextArea('untuk_keperluan', 'Untuk Keperluan') !!}

				{{-- {!! Form::mySelect('kategori', 'Kategori', config('variables.kategori'), null, ['class' => 'form-control select2']) !!} --}}

		</div>  
	</div>
</div>