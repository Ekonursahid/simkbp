@extends('admin.default')

@section('page-header')
	Jabatan <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['JabatanController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.jabatan.form')

		<button type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		
	{!! Form::close() !!}
	
@stop
