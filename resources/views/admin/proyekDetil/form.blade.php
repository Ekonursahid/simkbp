<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">
				@if(auth()->user()->jabatan->kode_jabatan === 'J004')

				{!! Form::myInput('text', 'kode_rab', 'Kode RAB') !!}
				{!! Form::myInput('text', 'jenis_pengeluaran', 'Jenis Pengeluaran') !!}
				{!! Form::myInput('number', 'kuantitas', 'Kuantitas') !!}
				{!! Form::myInput('number', 'harga_satuan', 'Harga Satuan') !!}
				{!! Form::myInput('text', 'satuan', 'satuan') !!}
				{!! Form::myInput('number', 'jumlah', 'Jumlah') !!}
				{!! Form::hidden('proyek_id', $idProyek) !!}

				@else

				{!! Form::myInput('text', 'kode_rab', 'Kode RAB', ['readonly']) !!}
				{!! Form::myInput('text', 'jenis_pengeluaran', 'Jenis Pengeluaran', ['readonly']) !!}
				{!! Form::myInput('number', 'kuantitas', 'Kuantitas', ['readonly']) !!}
				{!! Form::myInput('number', 'harga_satuan', 'Harga Satuan', ['readonly']) !!}
				{!! Form::myInput('text', 'satuan', 'satuan', ['readonly']) !!}
				{!! Form::myInput('number', 'jumlah', 'Jumlah', ['readonly']) !!}
				{!! Form::hidden('proyek_id', $idProyek) !!}

				<hr>
				 
				{!! Form::mySelect('status_proyek', 'Status Proyek', config('variables.status_proyek'), null, ['class' => 'form-control select2']) !!}
				@endif
				
		</div>  
	</div>
</div>