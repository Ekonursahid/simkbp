@php
    $r = \Route::current()->getAction();
    $route = (isset($r['as'])) ? $r['as'] : '';
@endphp

{{-- <li class="nav-item mT-30">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.dash') ? 'active' : '' }}" href="{{ route(ADMIN . '.dash') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-home"></i>
        </span>
        <span class="title">Dashboard</span>
    </a>
</li> --}}

@if(auth()->user()->jabatan->kode_jabatan === "J001") <!-- administrator -->
<li class="nav-item dropdown">
    <a class="dropdown-toggle" href="javascript:void(0);">
        <span class="icon-holder">
            <i class="c-brown-500 ti-user"></i>
        </span>
        <span class="title">Manajemen User</span>
        
        <span class="arrow">
            <i class="ti-angle-right"></i>
        </span>
    </a>
    <ul class="dropdown-menu">
        <li>
            <a class="sidebar-link {{ starts_with($route, ADMIN . '.users') ? 'active' : '' }}" href="{{ route(ADMIN . '.users.index') }}">
                Data User
            </a>
        </li>
        <li>
        {{-- <a class="sidebar-link {{ starts_with($route, ADMIN . '.menu') ? 'active' : '' }}" href="{{ route(ADMIN . '.menu.index') }}">
                Data Menu
            </a>
        </li> --}}
    </ul>
</li>
<li class="nav-item dropdown">
    <a class="dropdown-toggle" href="javascript:void(0);">
        <span class="icon-holder">
            <i class="c-red-500 ti-direction-alt"></i>
        </span>
        <span class="title">Data Referensi</span>
        
        <span class="arrow">
            <i class="ti-angle-right"></i>
        </span>
    </a>
    <ul class="dropdown-menu">
        <li>
            <a class="sidebar-link {{ starts_with($route, ADMIN . '.jabatan') ? 'active' : '' }}" href="{{ route(ADMIN . '.jabatan.index') }}">
                Data Jabatan
            </a>
        </li>
        <li>
        {{-- <a class="sidebar-link {{ starts_with($route, ADMIN . '.rekening') ? 'active' : '' }}" href="{{ route(ADMIN . '.rekening.index') }}">
                Rekening Bank
            </a>
        </li> --}}
    </ul>
</li>

@elseif(auth()->user()->jabatan->kode_jabatan === 'J002') <!-- HRD -->
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.karyawan') ? 'active' : '' }}" href="{{ route(ADMIN . '.karyawan.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-user"></i>
        </span>
        <span class="title">Data Karyawan</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.cuti') ? 'active' : '' }}" href="{{ route(ADMIN . '.cuti.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-share"></i>
        </span>
        <span class="title">Form Cuti Karyawan</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.izin') ? 'active' : '' }}" href="{{ route(ADMIN . '.izin.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-share"></i>
        </span>
        <span class="title">Form Permohonan Izin</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.users') ? 'active' : '' }}" href="{{ route(ADMIN . '.users.index') }}">
        <span class="icon-holder">
            <i class="c-purple-500 ti-notepad"></i>
        </span>
        <span class="title">Rekap Absensi</span>
    </a>
</li>

@elseif(auth()->user()->jabatan->kode_jabatan === 'J003') <!-- Operator HRD -->
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.cuti') ? 'active' : '' }}" href="{{ route(ADMIN . '.cuti.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-share"></i>
        </span>
        <span class="title">Form Cuti Karyawan</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.izin') ? 'active' : '' }}" href="{{ route(ADMIN . '.izin.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-share"></i>
        </span>
        <span class="title">Form Permohonan Izin</span>
    </a>
</li>
<!-- <li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.perjalananDinas') ? 'active' : '' }}" href="{{ route(ADMIN . '.perjalananDinas.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-share"></i>
        </span>
        <span class="title">Form Perjalanan Dinas</span>
    </a>
</li> -->


@elseif(auth()->user()->jabatan->kode_jabatan === 'kasir')
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.uudp') ? 'active' : '' }}" href="{{ route(ADMIN . '.uudp.index') }}">
        <span class="icon-holder">
            <i class="c-red-500 ti-list"></i>
        </span>
        <span class="title">Daftar UUDP</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.users') ? 'active' : '' }}" href="{{ route(ADMIN . '.users.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-pencil-alt"></i>
        </span>
        <span class="title">Form BKK dan BKM</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.users') ? 'active' : '' }}" href="{{ route(ADMIN . '.users.index') }}">
        <span class="icon-holder">
            <i class="c-purple-500 ti-notepad"></i>
        </span>
        <span class="title">Laporan Reumbruise</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.users') ? 'active' : '' }}" href="{{ route(ADMIN . '.users.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-pencil-alt"></i>
        </span>
        <span class="title">Form LPUM</span>
    </a>
</li>

@elseif(auth()->user()->jabatan->kode_jabatan === 'J004' || auth()->user()->jabatan->kode_jabatan === 'J005') <!-- manager keuangan & operator keuangan-->

<li class="nav-item mT-30">
    <a class="sidebar-link " href="{{ route(ADMIN . '.budgeting.dashboard') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-home"></i>
        </span>
        <span class="title">Dashboard</span>
    </a>
</li>

<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.budgeting') ? 'active' : '' }}" href="{{ route(ADMIN . '.budgeting.index') }}">
        <span class="icon-holder">
            <i class="c-red-500 ti-wallet"></i>
        </span>
        <span class="title">Budgeting</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.nonProyek') ? 'active' : '' }}" href="{{ route(ADMIN . '.nonProyek.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-share"></i>
        </span>
        <span class="title">Pengajuan NonProyek</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.proyek') ? 'active' : '' }}" href="{{ route(ADMIN . '.proyek.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-share"></i>
        </span>
        <span class="title">Pengajuan Proyek</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.uudp') ? 'active' : '' }}" href="{{ route(ADMIN . '.uudp.index') }}">
        <span class="icon-holder">
            <i class="c-purple-500 ti-notepad"></i>
        </span>
        <span class="title">Laporan UUDP</span>
    </a>
</li>

@elseif(auth()->user()->role === 'bendahara')
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.users') ? 'active' : '' }}" href="{{ route(ADMIN . '.users.index') }}">
        <span class="icon-holder">
            <i class="c-black-500 ti-book"></i>
        </span>
        <span class="title">Daftar Buku Bank</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.users') ? 'active' : '' }}" href="{{ route(ADMIN . '.users.index') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-share"></i>
        </span>
        <span class="title">Pengajuan Gaji & PPH21</span>
    </a>
</li>
@endif