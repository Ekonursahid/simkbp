<li>
    <a href="" class='peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100'>
        <div class="peer mR-15">
            <img class="w-3r bdrs-50p" src="/images/1.jpg" alt="">
        </div>
        <div class="peer peer-greed">
            <span>
                <span class="fw-500">John Doe</span>
                <span class="c-grey-600">{{ $notification->type }}<span class="text-dark">post</span>
                </span>
            </span>
            <p class="m-0">
                <small class="fsz-xs">5 mins ago</small>
            </p>
        </div>
    </a>
</li>