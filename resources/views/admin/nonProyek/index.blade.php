@extends('admin.default')

@section('page-header')
    NonProyek <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')
    {{-- @if(auth()->user()->jabatan->kode_jabatan == 'J004') --}}
    <div class="mB-20">
        <a href="{{ route(ADMIN . '.nonProyek.create') }}" class="btn btn-info">
            <i class="ti-plus"></i> {{ trans('app.add_button') }}
        </a>
    </div>
    {{-- @endif --}}

    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nama Pekerjaan</th>
                    <th>Tanggal Proyek</th>
                    <th>Untuk Keperluan</th>
                    <th>Status</th>
                    <th style="text-align:right;">Jumlah Dana Pengajuan</th>
                    <th>Action</th>
                </tr>
            </thead>
            
            <tfoot>
                <tr>
                    <th>Nama Pekerjaan</th>
                    <th>Tanggal Proyek</th>
                    <th>Untuk Keperluan</th>
                    <th>Status</th>
                    <th style="text-align:right;">Jumlah Dana Pengajuan</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            
            <tbody>
                @foreach ($items as $item)
                    <tr>
                        <td><a href="{{ route(ADMIN . '.nonProyek.edit', $item->id) }}">{{ $item->nama_pekerjaan }}</a></td>
                        <td>
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->format('d-M-Y') }} - 
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->diffForHumans(\Carbon\Carbon::now()) }}
                        </td>
                        <td>{{ $item->untuk_keperluan }}</td>
                        @if($item->status_non_proyek === 1)
                            <td><span class="badge bgc-green-50 c-green-700 p-10 lh-0 tt-c badge-pill">Distujui</span></td>
                        @elseif($item->status_non_proyek === 0)
                            <td><span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">Tidak Disetujui</span></td>
                        @else 
                            <td><span class="badge bgc-purple-50 c-purple-700 p-10 lh-0 tt-c badge-pill">Sedang Diproses</span></td>
                        @endif
                        <td style="text-align: right;">Rp {{ number_format($item->jumlah_dana_ajukan, 0, ',', ',') }}</td>
                        <td>
                            <ul class="list-inline">
                                @if(auth()->user()->jabatan->kode_jabatan === 'J004')
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.nonProyek.show', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>
                                <li class="list-inline-item">
                                    {!! Form::open([
                                        'class'=>'delete',
                                        'url'  => route(ADMIN . '.nonProyek.destroy', $item->id), 
                                        'method' => 'DELETE',
                                        ]) 
                                    !!}

                                        <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><span class="ti-trash"></span></button>
                                        
                                    {!! Form::close() !!}
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.nonProyek.print', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-success btn-sm"><span class="ti-printer"></span></a></li>
                                @else 
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.nonProyek.edit', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm">Approval</a></li>
                                @endif
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        
        </table>
    </div>

@endsection