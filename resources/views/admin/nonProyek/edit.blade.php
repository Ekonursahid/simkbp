@extends('admin.default')

@section('page-header')
	NonProyek <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['NonProyekController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.nonProyek.form')

		@if(auth()->user()->jabatan == 'J004')
			<button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		@else
			<button name="submintbutton" value="save" type="submit" class="btn btn-primary">Approve</button>
		@endif
	{!! Form::close() !!}

	@include('admin.nonProyekDetil.index')
	
@stop
