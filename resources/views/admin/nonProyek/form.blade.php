<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">

				{!! Form::myInput('text', 'nama_pekerjaan', 'Nama Pekerjaan') !!}

				@if(\Request::is('admin/nonProyek/create'))
					{!! Form::myInput('textbox', 'jumlah_dana_ajukan_display', 'Jumlah Dana yang Diajukan (Rp)', ['id' => 'currencyField']) !!}
				@else
					{!! Form::myInput('textbox', 'jumlah_dana_ajukan', 'Jumlah Dana yang Diajukan (Rp)', ['id' => 'currencyField']) !!}
				@endif

				{!! Form::hidden('jumlah_dana_ajukan', null, ['id' => 'jumlah_dana_ajukan_value']) !!}

				{!! Form::myTextArea('untuk_keperluan', 'Untuk Keperluan') !!}

				@if(auth()->user()->jabatan == 'J005')
				
				{!! Form::mySelect('status_non_proyek', 'Status Non Proyek', config('variables.status_cuti'), null, ['class' => 'form-control select2']) !!}
				{{-- {!! Form::myInput('text', 'diajukan_oleh', 'Diajukan Oleh') !!}
				{!! Form::myInput('text', 'menyetujui', 'Menyetujui') !!}
				{!! Form::myInput('text', 'bendahara', 'Bendahara') !!} --}}
				{!! Form::myInput('text', 'manager_keuangan', 'Manager Keuangan', [], auth()->user()->name ) !!}

				@endif 
		</div>  
	</div>
</div>

@section('js')
	<script> 
		document.getElementById("currencyField").onblur =function (){    

			//number-format the user input
			this.value = parseFloat(this.value.replace(/,/g, ""))
							.toFixed(2)
							.toString()
							.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

			//set the numeric value to a number input
			document.getElementById("jumlah_dana_ajukan_value").value = this.value.replace(/,/g, "")

		}
	</script>
@endsection