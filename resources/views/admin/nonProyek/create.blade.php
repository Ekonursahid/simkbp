@extends('admin.default')

@section('page-header')
	NonProyek <small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
	{!! Form::open([
			'action' => ['NonProyekController@store'],
			'files' => true
		])
	!!}

		@include('admin.nonProyek.form')

		<button name="submitbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
		
        <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
	{!! Form::close() !!}
	
@stop
