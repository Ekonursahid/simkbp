<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">
			{!! Form::myInput('text', 'kode_menu', 'Kode Menu') !!}
		
				{!! Form::myInput('text', 'nama_menu', 'Nama Menu') !!}

				{!! Form::mySelect('icon', 'Icon', config('variables.icon') , null, ['class' => 'form-control select2']) !!}

				{!! Form::myInput('text', 'url', 'Url') !!}
		
				{!! Form::mySelect('status', 'Status', config('variables.status'), null, ['class' => 'form-control select2']) !!}
		
		</div>  
	</div>
</div>