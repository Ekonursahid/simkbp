@extends('admin.default')

@section('page-header')
	Menu <small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
	{!! Form::open([
			'action' => ['MenuController@store'],
			'files' => false
		])
	!!}

		@include('admin.menu.form')

		<button type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
        <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
		
	{!! Form::close() !!}
	
@stop
