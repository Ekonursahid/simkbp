@extends('admin.default')

@section('page-header')
	Menu <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['MenuController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.menu.form')

		<button type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		
	{!! Form::close() !!}
	
@stop
