@extends('admin.default')

@section('page-header')
	UudpDetil <small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
	{!! Form::open([
			'url' => route(ADMIN . '.uudp.uudpDetil.store', ['idUudp' => $idUudp]),
			'files' => true
		])
	!!}

		@include('admin.uudpDetil.form')

		<button name="submitbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
		
        <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>

	{!! Form::close() !!}
	
@stop
