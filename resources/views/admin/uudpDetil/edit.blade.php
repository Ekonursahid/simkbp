@extends('admin.default')

@section('page-header')
	UudpDetil <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'url' => route(ADMIN . '.uudp.uudpDetil.update', ['idUudp' => $idUudp, 'id' => $item->id]),
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.uudpDetil.form')

		<button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		<a href="{{ url()->previous() }}" class="btn btn-success">Back</a>
		
	{!! Form::close() !!}
	
@stop
