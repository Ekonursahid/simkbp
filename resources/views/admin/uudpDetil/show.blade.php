@extends('admin.default')

@section('page-header')
	UudpDetil <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['UudpDetilController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.uudpDetil.form')

		{{-- <button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button> --}}
		
	{!! Form::close() !!}
	
	@include('admin.uudpDetilDetil.index')
	
@stop
