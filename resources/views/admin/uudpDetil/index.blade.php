{{-- @if(auth()->user()->role == 'operator') --}}
<div class="mB-20 mT-20">
    <a href="{{ route(ADMIN . '.uudp.uudpDetil.create', $item->id) }}" class="btn btn-info">
        <i class="ti-plus"></i> {{ trans('app.add_button') }}
    </a>
</div>
{{-- @endif --}}

<div class="bgc-white bd bdrs-3 p-20 mB-20">
    <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Kode RAB</th>
                <th>Jenis Pengeluaran</th>
                <th>Qty</th>
                <th>Satuan</th>
                <th>Harga Satuan</th>
                <th>Jumlah (Rp)</th>
                <th>Status Uudp</th>
                <th>Action</th>
            </tr>
        </thead>
        
        <tfoot>
            <tr>
                <th>Kode RAB</th>
                <th>Jenis Pengeluaran</th>
                <th>Qty</th>
                <th>Satuan</th>
                <th>Harga Satuan</th>
                <th>Jumlah (Rp)</th>
                <th>Status Uudp</th>
                <th>Action</th>
            </tr>
        </tfoot>
        
        <tbody>
            @foreach ($item->uudpDetil as $itemDetil)
                <tr>
                    <td><a href="{{ route(ADMIN . '.uudp.uudpDetil.edit', ['idUudp' => $item->id, 'id' => $itemDetil->id]) }}">{{ $itemDetil->kode_rab }}</a></td>
                    <td>{{ $itemDetil->jenis_pengeluaran }}</td>
                    <td>{{ $itemDetil->kuantitas }}</td>
                    <td>{{ $itemDetil->satuan }}</td>
                    <td>{{ $itemDetil->harga_satuan }}</td>
                    <td>{{ $itemDetil->jumlah }}</td>
                    @if($itemDetil->status_uudp === 1)
                        <td><span class="badge bgc-green-50 c-green-700 p-10 lh-0 tt-c badge-pill">Diterima</span></td>
                    @elseif($item->status_cuti === 0)
                        <td><span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">Ditolak</span></td>
                    @else 
                        <td><span class="badge bgc-purple-50 c-purple-700 p-10 lh-0 tt-c badge-pill">Sedang Diproses</span></td>
                    @endif
                    <td>
                        <ul class="list-inline">
                            @if(auth()->user()->role === 'manager keuangan')
                            <li class="list-inline-item">
                                <a href="{{ route(ADMIN . '.uudp.uudpDetil.edit', ['idUudp' => $item->id, 'id' => $itemDetil->id]) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm">Approval</span></a></li>
                            @else 
                            <li class="list-inline-item">
                                <a href="{{ route(ADMIN . '.uudp.uudpDetil.edit', ['idUudp' => $item->id, 'id' => $itemDetil->id]) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>

                            
                            <li class="list-inline-item">
                                {!! Form::open([
                                    'class'=>'delete',
                                    'url'  => route(ADMIN . '.uudpDetil.destroy', $itemDetil->id), 
                                    'method' => 'DELETE',
                                    ]) 
                                !!}

                                    <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i class="ti-trash"></i></button>
                                    
                                {!! Form::close() !!}
                            </li>
                            @endif
                            {{-- <li class="list-inline-item">
                                <a href="{{ route(ADMIN . '.uudpDetil.show', $itemDetil->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-success btn-sm"><span class="ti-printer"></span></a></li> --}}
                            
                        </ul>
                    </td>
                </tr>
            @endforeach
        </tbody>
    
    </table>
</div>
