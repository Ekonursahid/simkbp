<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">
				{!! Form::myInput('text', 'nik', 'NIK') !!}

				{!! Form::myInput('text', 'nama_karyawan', 'Nama Lengkap') !!}

				{!! Form::myTextArea('alamat', 'Alamat') !!}
		
				{!! Form::mySelect('status', 'Status', config('variables.status_karyawan'), null, ['class' => 'form-control select2']) !!}

				{!! Form::mySelect('department', 'Department', config('variables.department'), null, ['class' => 'form-control select2']) !!}

				{!! Form::mySelect('jabatan', 'Jabatan', config('variables.jabatan'), null, ['class' => 'form-control select2']) !!}
		
				{!! Form::myDate('date', 'tanggal_masuk', 'Tanggal Masuk') !!}

				{!! Form::myFile('avatar', 'Foto') !!}
		
				{!! Form::myTextArea('bio', 'Bio') !!}
		</div>  
	</div>
</div>