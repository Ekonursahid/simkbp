
<div class="mB-20 mT-20">
    @if(auth()->user()->jabatan->kode_jabatan == 'J004')
        <a href="{{ route(ADMIN . '.nonProyek.nonProyekDetil.create', $item->id) }}" class="btn btn-info">
            <i class="ti-plus"></i> {{ trans('app.add_button') }}
        </a>
    @endif
</div>


<div class="bgc-white bd bdrs-3 p-20 mB-20">
    <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Kode RAB</th>
                <th>Jenis Pengeluaran</th>
                <th>Qty</th>
                <th>Satuan</th>
                <th>Harga Satuan</th>
                <th>Jumlah (Rp)</th>
                <th>Action</th>
            </tr>
        </thead>
        
        <tfoot>
            <tr>
                <th>Kode RAB</th>
                <th>Jenis Pengeluaran</th>
                <th>Qty</th>
                <th>Satuan</th>
                <th>Harga Satuan</th>
                <th>Jumlah (Rp)</th>
                <th>Action</th>
            </tr>
        </tfoot>
        
        <tbody>
            @foreach ($item->nonProyekDetil as $itemDetil)
                <tr>
                    <td><a href="{{ route(ADMIN . '.nonProyek.nonProyekDetil.edit', ['idNonProyek' => $item->id, 'id' => $itemDetil->id]) }}">{{ $itemDetil->kode_rab }}</a></td>
                    <td>{{ $itemDetil->jenis_pengeluaran }}</td>
                    <td>{{ $itemDetil->kuantitas }}</td>
                    <td>{{ $itemDetil->satuan }}</td>
                    <td>{{ $itemDetil->harga_satuan }}</td>
                    <td>{{ $itemDetil->jumlah }}</td>
                    <td>
                        <ul class="list-inline">
                            
                            <li class="list-inline-item">
                                <a href="{{ route(ADMIN . '.nonProyek.nonProyekDetil.edit', ['idNonProyek' => $item->id, 'id' => $itemDetil->id]) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>

                            
                            <li class="list-inline-item">
                                {!! Form::open([
                                    'class'=>'delete',
                                    'url'  => route(ADMIN . '.nonProyekDetil.destroy', $itemDetil->id), 
                                    'method' => 'DELETE',
                                    ]) 
                                !!}

                                    <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i class="ti-trash"></i></button>
                                    
                                {!! Form::close() !!}
                            </li>
                            {{-- <li class="list-inline-item">
                                <a href="{{ route(ADMIN . '.nonProyekDetil.show', $itemDetil->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-success btn-sm"><span class="ti-printer"></span></a></li> --}}
                            
                        </ul>
                    </td>
                </tr>
            @endforeach
        </tbody>
    
    </table>
</div>
