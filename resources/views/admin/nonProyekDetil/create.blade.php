@extends('admin.default')

@section('page-header')
	NonProyekDetil <small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
	{!! Form::open([
			'url' => route(ADMIN . '.nonProyek.nonProyekDetil.store', ['idNonProyek' => $idNonProyek]),
			'files' => true
		])
	!!}

		@include('admin.nonProyekDetil.form')

		<button name="submitbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
        <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
		
	{!! Form::close() !!}
	
@stop
