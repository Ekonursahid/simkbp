<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">

				{!! Form::myInput('text', 'no_pengajuan', 'No Form Pengajuan') !!}

				{!! Form::myInput('date', 'tanggal_pengajuan', 'Tanggal Pengajuan', [] ,$item->tanggal_pengajuan->format('Y-m-d')) !!}

				{!! Form::myInput('text', 'nama', 'Nama') !!}

				{!! Form::myInput('text', 'nama_pekerjaan', 'Nama Pekerjaan') !!}

				@if(\Request::is('admin/budgeting/create'))
					{!! Form::myInput('textbox', 'jumlah_pengajuan_display', 'Jumlah Dana yang Diajukan (Rp)', ['id' => 'currencyField']) !!}
				@else
					{!! Form::myInput('textbox', 'jumlah_pengajuan', 'Jumlah Dana yang Diajukan (Rp)', ['id' => 'currencyField']) !!}
				@endif

				{!! Form::hidden('jumlah_pengajuan', null, ['id' => 'jumlah_pengajuan_value']) !!}

		</div>  
	</div>
</div>

@section('js')
	<script> 
		document.getElementById("currencyField").onblur =function (){    

			//number-format the user input
			this.value = parseFloat(this.value.replace(/,/g, ""))
							.toFixed(2)
							.toString()
							.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

			//set the numeric value to a number input
			document.getElementById("jumlah_pengajuan_value").value = this.value.replace(/,/g, "")

		}
	</script>
@endsection