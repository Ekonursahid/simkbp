@extends('admin.default')

@section('page-header')
	Uudp <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['UudpController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.uudp.form')

		<button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		
	{!! Form::close() !!}
	
@stop
