@extends('admin.default')

@section('page-header')
    Uudp <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')
    {{-- @if(auth()->user()->role == 'operator') --}}
    <div class="mB-20">
        <a href="{{ route(ADMIN . '.uudp.create') }}" class="btn btn-info">
            <i class="ti-plus"></i> {{ trans('app.add_button') }}
        </a>
    </div>
    {{-- @endif --}}

    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No Form Pengajuan</th>
                    <th>Tanggal Pengajuan</th>
                    <th>Nama Pekerjaan</th>
                    <th style="text-align:right;">Jumlah Pengajuan</th>
                    <th>Action</th>
                </tr>
            </thead>
            
            <tfoot>
                <tr>
                    <th>No Form Pengajuan</th>
                    <th>Tanggal Pengajuan</th>
                    <th>Nama Pekerjaan</th>
                    <th style="text-align:right;">Jumlah Pengajuan</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            
            <tbody>
                @foreach ($items as $item)
                    <tr>
                        <td><a href="{{ route(ADMIN . '.uudp.edit', $item->id) }}">{{ $item->no_pengajuan }}</a></td>
                        <td>
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->tanggal_pengajuan)->format('d-M-Y') }} - 
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->tanggal_pengajuan)->diffForHumans(\Carbon\Carbon::now()) }}
                        </td>
                        <td>{{ $item->nama_pekerjaan }}</td>
                        <td style="text-align:right;">Rp {{ number_format($item->jumlah_pengajuan, 0, ',', ',') }}</td>
                        <td>
                            <ul class="list-inline">
                                
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.uudp.show', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>
                                
                                <li class="list-inline-item">
                                    {!! Form::open([
                                        'class'=>'delete',
                                        'url'  => route(ADMIN . '.uudp.destroy', $item->id), 
                                        'method' => 'DELETE',
                                        ]) 
                                    !!}

                                        <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><span class="ti-trash"></span></button>
                                        
                                    {!! Form::close() !!}
                                </li>
                                {{-- <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.uudp.show', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-success btn-sm"><span class="ti-printer"></span></a></li>
                                 --}}
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        
        </table>
    </div>

@endsection