@extends('admin.default')

@section('page-header')
    Budgeting <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')
    {{-- @if(auth()->user()->jabatan->kode_jabatan == 'J004') --}}
    <div class="mB-20">
        <a href="{{ route(ADMIN . '.budgeting.create') }}" class="btn btn-info">
            <i class="ti-plus"></i> {{ trans('app.add_button') }} 
        </a>
    </div>
    {{-- @endif --}}

    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Kode</th>
                    <th>Nama Pekerjaan</th>
                    <th style="text-align:right;">Anggaran</th>
                    <th style="text-align:right;">Alokasi</th>
                    <th style="text-align:right;">Sisa</th>
                    <th>Action</th>
                </tr>
            </thead>
            
            <tfoot>
                <tr>
                    <th>Kode</th>
                    <th>Nama Pekerjaan</th>
                    <th style="text-align:right;">Anggaran</th>
                    <th style="text-align:right;">Alokasi</th>
                    <th style="text-align:right;">Sisa</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            
            <tbody>
                @foreach ($items as $item)
                    <tr>
                        <td><a href="{{ route(ADMIN . '.budgeting.show', $item->id) }}">{{ $item->kode_budget }}</a></td>
                        <td>{{ $item->nama_pekerjaan }}</td>
                        
                        <td style="text-align: right;">Rp {{ number_format($item->jumlah_dana_ajukan, 0, ',', ',') }},00</td>
                        <td style="text-align: right;">Rp. {{ number_format($item->budgetingDetil->sum('total_personil') + $item->budgetingDetil->sum('total_non_personil'), 0, ',', ',') }},00</td>
                        <td style="text-align: right;">Rp. {{ number_format($item->jumlah_dana_ajukan - ($item->budgetingDetil->sum('total_personil') + $item->budgetingDetil->sum('total_non_personil')), 0, ',', ',') }},00</td>
                        <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.budgeting.show', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>

                                @if(auth()->user()->jabatan->kode_jabatan === 'J004')
                                <li class="list-inline-item">
                                    {!! Form::open([
                                        'class'=>'delete',
                                        'url'  => route(ADMIN . '.budgeting.destroy', $item->id), 
                                        'method' => 'DELETE',
                                        ]) 
                                    !!}

                                        <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><span class="ti-trash"></span></button>
                                        
                                    {!! Form::close() !!}
                                </li>
                                {{-- <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.budgeting.print', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-success btn-sm"><span class="ti-printer"></span></a></li> --}}
                                @endif
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        
        </table>
    </div>

@endsection