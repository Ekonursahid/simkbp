@extends('admin.default')

@section('page-header')
	Budgeting <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['BudgetingController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.budgeting.form')

		<button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		
	{!! Form::close() !!}
	
	@include('admin.budgetingDetil.index')
	
@stop
