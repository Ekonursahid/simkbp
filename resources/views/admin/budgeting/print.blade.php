<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        table {
            border: 1px solid black;
            width: 100%;
            margin-top: 50px;
        }
        thead {
            text-align: left;
            /* border-top: 1px solid black; */
        }
        th {
            padding: 20px;
            /* border-top: 1px solid black; */
        }
        td {
            padding: 10px;
            border-top: 1px solid black;
            border-right: 1px solid black;
            /* border-right: 1px solid black; */
        }
    </style>
</head>
<body>
    <div class="container">
        8. Form Pengajuan Dana Operasional Proyek
        <table>
            <thead>
                <tr>
                    <th colspan="7"> PT. BHUMI PRASAJA <br> BANDUNG</th>
                </tr>
                <tr>
                    <th colspan="7" style="text-align:right;"> No. : ........................................</th> 
                </tr>
                <tr>
                    <th colspan="7" style="text-align:right;"> Tanggal. : ........................................</th> 
                </tr>
                <tr>
                    <td colspan="7" style="text-align:center;"> <strong>FORM PENGAJUAN DANA OPERASIONAL PROYEK  </strong> </td> 
                </tr>
                <tr>
                    <td colspan="2"> Nama Pekerjaan : </td> 
                    <td colspan="5"> ... </td> 
                </tr>
                <tr>
                    <td colspan="2"> Kode Proyek : </td> 
                    <td colspan="5"> ... </td> 
                </tr>
                <tr>
                    <td colspan="2"> Jumlah Dana yang diajukan : </td> 
                    <td colspan="5"> ... </td> 
                </tr>
                <tr>
                    <td colspan="2"> Untuk Keperluan : </td> 
                    <td colspan="5"> ... </td> 
                </tr>
                <tr>
                    <td colspan="2"> Kategori : </td> 
                    <td colspan="5"> ... </td> 
                </tr>
                <tr>
                    <td style="text-align:center;" colspan="7"><strong>RINCIAN PENGAJUAN</strong></td>
                </tr>
            </thead>
            @if($item->jenis_budget === 'personil')
                <tbody>
                    <tr>
                        <td>No.</td>
                        <td>Nama Personil</td>
                        <td>Jumlah Bulan</td>
                        <td>Honor Satuan</td>
                        <td>Total</td>
                    </tr>
                    @foreach ($item->budgetingDetil as $key => $itemDetil)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $itemDetil->nama_personil }}</td>
                        <td>{{ $itemDetil->jumlah_bulan }}</td>
                        <td>{{ $itemDetil->honor_satuan }}</td>
                        <td>{{ $itemDetil->jumlah_bulan * $itemDetil->honor_satuan }}</td>
                    </tr>
                    @endforeach
                </tbody>
            @else 
                <tbody>
                    <tr>
                        <td>No.</td>
                        <td>Nama Kegiatan</td>
                        <td>Nama Unit</td>
                        <td>Harga Satuan</td>
                        <td>Jumlah</td>
                        <td>Total</td>
                    </tr>
                    @foreach ($item->budgetingDetil as $key => $itemDetil)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $itemDetil->nama_kegiatan }}</td>
                        <td>{{ $itemDetil->nama_unit }}</td>
                        <td>{{ $itemDetil->harga_satuan }}</td>
                        <td>{{ $itemDetil->jumlah }}</td>
                        <td>{{ $itemDetil->harga_satuan * $itemDetil->jumlah }}</td>
                    </tr>
                    @endforeach
                </tbody>
            @endif
        </table>
    </div>
    
</body>
</html>