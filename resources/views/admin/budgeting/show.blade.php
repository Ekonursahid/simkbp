@extends('admin.default')

@section('page-header')
	Budgeting <small>{{ trans('app.update_item') }}</small>

	
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['BudgetingController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.budgeting.form')

		{{-- <button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button> --}}
		
	{!! Form::close() !!}
	
	{{-- anggaran --}}
	<div class="masonry-sizer col-md-6"></div>
	<div class="masonry-item  w-100">
		<div class="row gap-20">
			<!-- #Toatl Visits ==================== -->
			<div class='col-md-4'>
				<div class="layers bd bgc-white p-20">
					<div class="layer w-100 mB-10">
						<h6 class="lh-1">Anggaran</h6>
					</div>
					<div class="layer w-100">
						<div class="peers ai-sb fxw-nw">
							{{-- <div class="peer peer-greed">
								<span id="sparklinedash"></span>
							</div> --}}
							<div class="peer">
								<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-green-50 c-green-500">
									Rp. {{ number_format($item->jumlah_dana_ajukan, 0, ',', ',') }},00
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- #Total Page Views ==================== -->
			<div class='col-md-4'>
				<div class="layers bd bgc-white p-20">
					<div class="layer w-100 mB-10">
						<h6 class="lh-1">Aktualisasi</h6>
					</div>
					<div class="layer w-100">
						<div class="peers ai-sb fxw-nw">
							{{-- <div class="peer peer-greed">
								<span id="sparklinedash2"></span>
							</div> --}}
							<div class="peer">
								<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-red-50 c-red-500">
									Rp. {{ number_format($item->budgetingDetil->sum('total_personil') + $item->budgetingDetil->sum('total_non_personil'), 0, ',', ',') }},00
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- #Unique Visitors ==================== -->
			<div class='col-md-4'>
				<div class="layers bd bgc-white p-20">
					<div class="layer w-100 mB-10">
						<h6 class="lh-1">Sisa</h6>
					</div>
					<div class="layer w-100">
						<div class="peers ai-sb fxw-nw">
							{{-- <div class="peer peer-greed">
								<span id="sparklinedash3"></span>
							</div> --}}
							<div class="peer">
								<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-purple-50 c-purple-500">
									Rp. {{ number_format($item->jumlah_dana_ajukan - ($item->budgetingDetil->sum('total_personil') + $item->budgetingDetil->sum('total_non_personil')), 0, ',', ',') }},00
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- #Bounce Rate ==================== -->
			{{-- <div class='col-md-3'>
				<div class="layers bd bgc-white p-20">
					<div class="layer w-100 mB-10">
						<h6 class="lh-1">Bounce Rate</h6>
					</div>
					<div class="layer w-100">
						<div class="peers ai-sb fxw-nw">
							<div class="peer peer-greed">
								<span id="sparklinedash4"></span>
							</div>
							<div class="peer">
								<span class="d-ib lh-0 va-m fw-600 bdrs-10em pX-15 pY-15 bgc-blue-50 c-blue-500">33%</span>
							</div>
						</div>
					</div>
				</div>
			</div> --}}
		</div>
	</div>
	<hr>
	
	
	@include('admin.budgetingDetil.indexPersonil')
	@include('admin.budgetingDetil.indexNonPersonil')
	
@stop
