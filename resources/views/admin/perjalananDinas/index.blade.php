@extends('admin.default')

@section('page-header')
    Perjalanan Dinas <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')
    @if(auth()->user()->role == 'operator')
    <div class="mB-20">
        <a href="{{ route(ADMIN . '.perjalananDinas.create') }}" class="btn btn-info">
            <i class="ti-plus"></i> {{ trans('app.add_button') }}
        </a>
    </div>
    @endif

    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Tanggal Perjalanan Dinas</th>
                    <th>Status</th>
                    <th>Alasan</th>
                    <th>Actions</th>
                </tr>
            </thead>
            
            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Tanggal PerjalananDinas</th>
                    <th>Status</th>
                    <th>Alasan</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
            
            <tbody>
                @foreach ($items as $item)
                    <tr>
                        <td><a href="{{ route(ADMIN . '.perjalananDinas.edit', $item->id) }}">{{ $item->karyawan->nama_karyawan }}</a></td>
                        <td>
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->tanggal_perjalananDinas)->format('d-M-Y') }} - 
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->tanggal_perjalananDinas)->diffForHumans(\Carbon\Carbon::now()) }}
                        </td>
                        @if($item->status_perjalananDinas === 1)
                            <td><span class="badge bgc-green-50 c-green-700 p-10 lh-0 tt-c badge-pill">Distujui</span></td>
                        @elseif($item->status_perjalananDinas === 0)
                            <td><span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">Tidak Disetujui</span></td>
                        @else 
                            <td><span class="badge bgc-purple-50 c-purple-700 p-10 lh-0 tt-c badge-pill">Sedang Diproses</span></td>
                        @endif
                        <td>{{ $item->alasan }}</td>
                        <td>
                            <ul class="list-inline">
                                @if(auth()->user()->role === 'operator')
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.perjalananDinas.edit', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>
                                @else 
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.perjalananDinas.edit', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm">approval</a></li>
                                @endif

                                @if(auth()->user()->role === 'operator')
                                <li class="list-inline-item">
                                    {!! Form::open([
                                        'class'=>'delete',
                                        'url'  => route(ADMIN . '.perjalananDinas.destroy', $item->id), 
                                        'method' => 'DELETE',
                                        ]) 
                                    !!}

                                        <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i class="ti-trash"></i></button>
                                        
                                    {!! Form::close() !!}
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.perjalananDinas.show', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-success btn-sm"><span class="ti-printer"></span></a></li>
                                @endif
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        
        </table>
    </div>

@endsection