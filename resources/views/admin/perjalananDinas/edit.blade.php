@extends('admin.default')

@section('page-header')
	Perjalanan Dinas <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($item, [
			'action' => ['PerjalananDinasController@update', $item->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.perjalananDinas.form')

		<button name="submintbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		
	{!! Form::close() !!}
	
@stop
