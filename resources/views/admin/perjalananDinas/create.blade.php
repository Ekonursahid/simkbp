@extends('admin.default')

@section('page-header')
	PerjalananDinas <small>{{ trans('app.add_new_item') }}</small>
@stop

@section('content')
	{!! Form::open([
			'action' => ['PerjalananDinasController@store'],
			'files' => true
		])
	!!}

		@include('admin.perjalananDinas.form')

		<button name="submitbutton" value="save" type="submit" class="btn btn-primary">{{ trans('app.add_button') }}</button>
        <a href="{{ url()->previous() }}" class="btn btn-danger">Cancel</a>
		
	{!! Form::close() !!}
	
@stop
