<?php

return [
    
    'boolean' => [
        '0' => 'No',
        '1' => 'Yes',
    ],

    'role' => [
        '0' => 'User',
        '10' => 'Admin',
    ],
    
    'status' => [
        '1' => 'Active',
        '0' => 'Inactive',
    ],

    'status_karyawan' => [
        'Karyawan Tetap' => 'Karyawan Tetap',
        'Karyawan Kontrak' => 'Karyawan Kontrak',
    ],

    'status_cuti' => [
        '0' => 'Tidak Disetujui',
        '1' => 'Disetujui',
    ],

    'status_proyek' => [
        '0' => 'Ditolak',
        '1' => 'Diterima',
    ],

    'jenis_budget' => [
        '0' => 'Non Personil',
        '1' => 'Personil',
    ],

    'department' => [
        'Paket PPL 4 2018' => 'Paket PPL 4 2018',
        'Paket PPL 4 2019' => 'Paket PPL 4 2019',
    ],

    'kategori' => [
        'Kategori 1' => 'Kategori 1',
        'Kategori 2' => 'Kategori 2',
    ],

    'jabatan' => [
        'Surveyor Operator' => 'Surveyor Operator',
        'Analis' => 'Analis',
    ],

    'atasan' => [
        'Khoirul Karim, S.T' => 'Khoirul Karim, S.T',
        'Drs. Suhardiman' => 'Drs. Suhardiman',
        'Kurwinda Latifah' => 'Kurwinda Latifah',
        'Riswan Rismawan' => 'Riswan Rismawan',
    ],

    'avatar' => [
        'public' => '/storage/avatar/',
        'folder' => 'avatar',
        
        'width'  => 400,
        'height' => 400,
    ],

    'icon' => [
        '0' => '&#92;e600; ti-wand',
        '1' => '&#xe601; ti-volume',
        '2' => '&#xe602; ti-users',
        '3' => '&#xe603; ti-unlock',
        '4' => '&#xe604; ti-unlink',
        '5' => '&#xe630; ti-trash',
    ],

    /*
    |------------------------------------------------------------------------------------
    | ENV of APP
    |------------------------------------------------------------------------------------
    */
    'APP_ADMIN' => 'admin',
    'APP_TOKEN' => env('APP_TOKEN', 'admin123456'),
];
