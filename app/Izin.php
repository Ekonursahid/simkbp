<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Izin extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'karyawan_id', 'tanggal_izin', 'alasan', 'diajukan_oleh', 'disetujui_oleh',
        'diketahui_oleh', 'status_izin'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            // 'karyawan_id' => "required",
            // 'tanggal_izin' => 'required',
            // 'alasan' => 'required',
            // 'diajukan_oleh' => 'required',
            // 'disetujui_oleh' => 'required',
            // 'diketahui_oleh' => 'required',
        ];

        return $commun;
    }

    public function karyawan() 
    {
        return $this->belongsTo('App\Karyawan');
    }
}
