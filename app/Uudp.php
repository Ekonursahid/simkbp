<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Uudp extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'nama_pekerjaan', 'no_pengajuan', 'jumlah_pengajuan', 'tanggal_pengajuan',
        'status_uudp' 
    ];

    // public function getTanggalPengajuanAttribute($date)
    // {
    //     $date = new \Carbon\Carbon($date);
    // }
    // protected $dates = ['tanggal_pengejuan'];

    public function getTanggalPengajuanAttribute($value)
    {
        $tgl = new Carbon($value);
        return $tgl;
    }

    // public function setTanggalPengajuanAttribute($value)
    // {
    //     $this->attributes['tanggal_pengajuan'] = Carbon::parse($value);
    // }
    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            // 'karyawan_id' => "required",
            // 'tanggal_izin' => 'required',
            // 'alasan' => 'required',
            // 'diajukan_oleh' => 'required',
            // 'disetujui_oleh' => 'required',
            // 'diketahui_oleh' => 'required',
        ];

        return $commun;
    }

    public function UudpDetil() 
    {
        return $this->hasMany('App\UudpDetil');
    }
}
