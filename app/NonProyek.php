<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class NonProyek extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_pekerjaan', 'jumlah_dana_ajukan', 'jumlah_dana_disetujui', 
        'untuk_keperluan', 'tanggal_disetujui', 'diajukan_oleh', 'menyetujui',
        'bendahara', 'manajer_keuangan', 'status_non_proyek'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            // 'karyawan_id' => "required",
            // 'tanggal_izin' => 'required',
            // 'alasan' => 'required',
            // 'diajukan_oleh' => 'required',
            // 'disetujui_oleh' => 'required',
            // 'diketahui_oleh' => 'required',
        ];

        return $commun;
    }

    public function NonProyekDetil() 
    {
        return $this->hasMany('App\NonProyekDetil');
    }
}
