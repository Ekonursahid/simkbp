<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class UudpDetil extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uudp_id', 'kode_rab', 'jenis_pengeluaran', 'kuantitas', 'satuan',
        'jumlah', 'harga_satuan', 'status_uudp'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            // 'karyawan_id' => "required",
            // 'tanggal_izin' => 'required',
            // 'alasan' => 'required',
            // 'diajukan_oleh' => 'required',
            // 'disetujui_oleh' => 'required',
            // 'diketahui_oleh' => 'required',
        ];

        return $commun;
    }

    public function uudp() 
    {
        return $this->belongsTo('App\Uudp');
    }
}
