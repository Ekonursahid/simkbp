<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_jabatan', 'jabatan', 'status',
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            'kode_jabatan'    => "required",
            'jabatan' => 'required',
            'status' => 'required',
        ];

        return $commun;
    }

    public function user() 
    {
        return $this->hasMany('App\User');
    }
}
