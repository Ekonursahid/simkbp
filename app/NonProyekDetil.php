<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class NonProyekDetil extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'non_proyek_id', 'kode_rab', 'jenis_pengeluaran', 'kuantitas', 'satuan',
        'jumlah', 'harga_satuan'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            // 'karyawan_id' => "required",
            // 'tanggal_izin' => 'required',
            // 'alasan' => 'required',
            // 'diajukan_oleh' => 'required',
            // 'disetujui_oleh' => 'required',
            // 'diketahui_oleh' => 'required',
        ];

        return $commun;
    }

    public function nonProyek() 
    {
        return $this->belongsTo('App\NonProyek');
    }
}
