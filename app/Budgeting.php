<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Budgeting extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_budget', 'jenis_budget', 'nama_pekerjaan', 'jumlah_dana_ajukan', 'jumlah_dana_disetujui', 
        'untuk_keperluan', 'kategori', 'tanggal_disetujui', 'diajukan_oleh', 'menyetujui',
        'bendahara'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            'nama_pekerjaan' => "required",
            'kode_budget' => 'required',
            'jumlah_dana_ajukan' => 'required',
            // 'diajukan_oleh' => 'required',
            // 'disetujui_oleh' => 'required',
            // 'diketahui_oleh' => 'required',
        ];

        return $commun;
    }

    public function BudgetingDetil() 
    {
        return $this->hasMany('App\BudgetingDetil');
    }
}
