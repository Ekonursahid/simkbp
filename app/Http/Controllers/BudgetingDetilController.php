<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\User;
use App\BudgetingDetil;
use App\Budgeting;
// use App\Notifications\RepliedToBudgetingDetil;

class BudgetingDetilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = BudgetingDetil::latest('updated_at')->get();

        return view('admin.budgetingDetil.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idBudgeting)
    {
        $this->idBudgeting = $idBudgeting;
        $budgeting = Budgeting::findOrFail($idBudgeting);

        return view('admin.budgetingDetil.create', compact('idBudgeting', 'budgeting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idBudgeting)
    {
        $this->validate($request, BudgetingDetil::rules());
        
        if($request->jenis_budget == '1')
        {
            $totalPersonil = $request->input('jumlah_bulan') * $request->input('honor_satuan');
            $request->request->add(['total_personil' => $totalPersonil]);
        }
        else
        {
            $totalNonPersonil = $request->input('harga_satuan') * $request->input('jumlah');
            $request->request->add(['total_non_personil' => $totalNonPersonil]);   
        }
        // dd($request);
        BudgetingDetil::create($request->all());

        return redirect()->route(ADMIN . '.budgeting.show', ['idBudgeting' => $idBudgeting])->withSuccess(trans('app.success_store'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = BudgetingDetil::findOrFail($id);
        return view('admin.budgetingDetil.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idBudgeting, $id)
    {
        $this->idBudgeting = $idBudgeting;
        $item = BudgetingDetil::findOrFail($id);
        $budgeting = Budgeting::findOrFail($idBudgeting);

        return view('admin.budgetingDetil.edit', compact('item', 'idBudgeting', 'budgeting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idBudgeting, $id)
    {
        $this->validate($request, BudgetingDetil::rules(true, $id));

        $item = BudgetingDetil::findOrFail($id);

        if($request->jenis_budget == '1')
        {
            $totalPersonil = $request->input('jumlah_bulan') * $request->input('honor_satuan');
            $request->request->add(['total_personil' => $totalPersonil]);
        }
        else
        {
            $totalNonPersonil = $request->input('harga_satuan') * $request->input('jumlah');
            $request->request->add(['total_non_personil' => $totalNonPersonil]);   
        }

        $item->update($request->all());

        return redirect()->route(ADMIN . '.budgeting.show', ['idBudgeting' => $idBudgeting])->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BudgetingDetil::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

