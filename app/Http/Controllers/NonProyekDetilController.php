<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\User;
use App\NonProyekDetil;
// use App\Notifications\RepliedToNonProyekDetil;

class NonProyekDetilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = NonProyekDetil::latest('updated_at')->get();

        return view('admin.nonProyekDetil.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idNonProyek)
    {
        $this->idNonProyek = $idNonProyek;
        return view('admin.nonProyekDetil.create', compact('idNonProyek'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idNonProyek)
    {
        $this->validate($request, NonProyekDetil::rules());
        
        NonProyekDetil::create($request->all());

        return redirect()->route(ADMIN . '.nonProyek.show', ['idNonProyek' => $idNonProyek])->withSuccess(trans('app.success_store'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = NonProyekDetil::findOrFail($id);
        return view('admin.nonProyekDetil.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idNonProyek, $id)
    {
        $this->idNonProyek = $idNonProyek;
        $item = NonProyekDetil::findOrFail($id);

        return view('admin.nonProyekDetil.edit', compact('item', 'idNonProyek'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idNonProyek, $id)
    {
        $this->validate($request, NonProyekDetil::rules(true, $id));

        $item = NonProyekDetil::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.nonProyek.show', ['idNonProyek' => $idNonProyek])->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NonProyekDetil::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

