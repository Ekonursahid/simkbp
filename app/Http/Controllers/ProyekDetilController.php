<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\User;
use App\ProyekDetil;
// use App\Notifications\RepliedToProyekDetil;

class ProyekDetilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = ProyekDetil::latest('updated_at')->get();

        return view('admin.proyekDetil.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idProyek)
    {
        $this->idProyek = $idProyek;
        return view('admin.proyekDetil.create', compact('idProyek'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idProyek)
    {
        $this->validate($request, ProyekDetil::rules());
        
        ProyekDetil::create($request->all());

        return redirect()->route(ADMIN . '.proyek.show', ['idProyek' => $idProyek])->withSuccess(trans('app.success_store'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = ProyekDetil::findOrFail($id);
        return view('admin.proyekDetil.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idProyek, $id)
    {
        $this->idProyek = $idProyek;
        $item = ProyekDetil::findOrFail($id);

        return view('admin.proyekDetil.edit', compact('item', 'idProyek'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idProyek, $id)
    {
        $this->validate($request, ProyekDetil::rules(true, $id));

        $item = ProyekDetil::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.proyek.show', ['idProyek' => $idProyek])->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProyekDetil::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

