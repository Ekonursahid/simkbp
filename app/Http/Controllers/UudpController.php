<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\User;
use App\Uudp;
use App\UudpDetil;
// use App\Notifications\RepliedToUudp;

class UudpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Uudp::latest('updated_at')->get();

        return view('admin.uudp.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = null;

        return view('admin.uudp.create', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->submitbutton == 'save')
        {
            $this->validate($request, Uudp::rules());
        
            Uudp::create($request->all());

            $users = User::where('role', 'HRD')->first();

            // $users->notify(new RepliedToUudp($request));

            return back()->withSuccess(trans('app.success_store'));
        }
        else 
        {
            //$item = Karyawan::findOrFail($request->karyawan_id);

            return view('admin.uudp.create', compact('item'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Uudp::findOrFail($id);
        //return dd($item->tanggal_pengajuan);
         return view('admin.uudp.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Uudp::findOrFail($id);

        //return dd($item->tanggal_pengajuan);

        return view('admin.uudp.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Uudp::rules(true, $id));

        $item = Uudp::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.uudp.index')->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Uudp::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

