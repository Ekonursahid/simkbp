<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cuti;
use App\Karyawan;

class CutiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Cuti::latest('updated_at')->get();

        return view('admin.cuti.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $karyawan = Karyawan::all()->pluck('nama_karyawan', 'id');
        $item = null;

        return view('admin.cuti.create', compact('karyawan', 'item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->submitbutton2 == 'save')
        {
            $this->validate($request, Cuti::rules());
            
            Cuti::create($request->all());

            return back()->withSuccess(trans('app.success_store'));

            //return dd($request);
        }
        else 
        {
            $karyawan = Karyawan::all()->pluck('nama_karyawan', 'id');
            $item = Karyawan::findOrFail($request->karyawan);

            return view('admin.cuti.create', compact('karyawan', 'item'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Cuti::findOrFail($id);
        return view('admin.cuti.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Cuti::findOrFail($id);
        $karyawan = Karyawan::all()->pluck('nama_karyawan', 'id');

        return view('admin.cuti.edit', compact('item', 'karyawan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Cuti::rules(true, $id));

        $item = Cuti::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.cuti.index')->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cuti::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

