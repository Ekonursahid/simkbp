<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\User;
use App\UudpDetil;
// use App\Notifications\RepliedToUudpDetil;

class UudpDetilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = UudpDetil::latest('updated_at')->get();

        return view('admin.uudpDetil.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idUudp)
    {
        $this->idUudp = $idUudp;
        return view('admin.uudpDetil.create', compact('idUudp'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idUudp)
    {
        $this->validate($request, UudpDetil::rules());
        
        UudpDetil::create($request->all());

        return redirect()->route(ADMIN . '.uudp.show', ['idUudp' => $idUudp])->withSuccess(trans('app.success_store'));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = UudpDetil::findOrFail($id);
        return view('admin.uudpDetil.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idUudp, $id)
    {
        $this->idUudp = $idUudp;
        $item = UudpDetil::findOrFail($id);

        return view('admin.uudpDetil.edit', compact('item', 'idUudp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idUudp, $id)
    {
        $this->validate($request, UudpDetil::rules(true, $id));

        $item = UudpDetil::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.uudp.show', ['idUudp' => $idUudp])->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UudpDetil::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

