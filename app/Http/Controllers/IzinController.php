<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\User;
use App\Izin;
use App\Karyawan;
use App\Notifications\RepliedToIzin;

class IzinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Izin::latest('updated_at')->get();

        return view('admin.izin.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $karyawan = Karyawan::all()->pluck('nama_karyawan', 'id');
        $item = null;

        return view('admin.izin.create', compact('karyawan', 'item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->submitbutton == 'save')
        {
            $this->validate($request, Izin::rules());
        
            Izin::create($request->all());

            $users = User::where('role', 'HRD')->first();

            $users->notify(new RepliedToIzin($request));

            return back()->withSuccess(trans('app.success_store'));
        }
        else 
        {
            $karyawan = Karyawan::all()->pluck('nama_karyawan', 'id');
            $item = Karyawan::findOrFail($request->karyawan_id);

            return view('admin.izin.create', compact('karyawan', 'item'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Izin::findOrFail($id);
        return view('admin.izin.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Izin::findOrFail($id);
        $karyawan = Karyawan::all()->pluck('nama_karyawan', 'id');

        return view('admin.izin.edit', compact('item', 'karyawan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Izin::rules(true, $id));

        $item = Izin::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.izin.index')->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Izin::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

