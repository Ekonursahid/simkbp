<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\User;
use App\Budgeting;
use App\BudgetingDetil;

// use App\Notifications\RepliedToBudgeting;

class BudgetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Budgeting::latest('updated_at')->get();

        return view('admin.budgeting.index', compact('items'));
    }

    public function dashboard()
    {
        $items = Budgeting::latest('updated_at')->get();

        return view('admin.budgeting.dashboard', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = null;

        return view('admin.budgeting.create', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->submitbutton == 'save')
        {
            $this->validate($request, Budgeting::rules());
        
            Budgeting::create($request->all());

            // $users = User::where('role', 'HRD')->first();

            // $users->notify(new RepliedToBudgeting($request));

            return back()->withSuccess(trans('app.success_store'));
        }
        else 
        {
            //$item = Karyawan::findOrFail($request->karyawan_id);

            return view('admin.budgeting.create', compact('item'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Budgeting::findOrFail($id);
        
        return view('admin.budgeting.show', compact('item'));
    }

    public function print($id)
    {
        $item = Budgeting::findOrFail($id);
        $budgeting = $item->jenis_budget;
        $i;
        return view('admin.budgeting.print', compact('item', 'budgeting', 'i'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Budgeting::findOrFail($id);

        return view('admin.budgeting.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Budgeting::rules(true, $id));

        $item = Budgeting::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.budgeting.index')->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Budgeting::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

