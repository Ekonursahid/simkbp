<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\User;
use App\PerjalananDinas;
use App\Karyawan;
// use App\Notifications\RepliedToPerjalananDinas;

class PerjalananDinasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = PerjalananDinas::latest('updated_at')->get();

        return view('admin.perjalananDinas.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $karyawan = Karyawan::all()->pluck('nama_karyawan', 'id');
        $item = null;

        return view('admin.perjalananDinas.create', compact('karyawan', 'item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->submitbutton == 'save')
        {
            $this->validate($request, PerjalananDinas::rules());
        
            PerjalananDinas::create($request->all());

            $users = User::where('role', 'HRD')->first();

            //$users->notify(new RepliedToPerjalananDinas($request));

            return back()->withSuccess(trans('app.success_store'));
        }
        else 
        {
            $karyawan = Karyawan::all()->pluck('nama_karyawan', 'id');
            $item = Karyawan::findOrFail($request->karyawan_id);

            return view('admin.perjalananDinas.create', compact('karyawan', 'item'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = PerjalananDinas::findOrFail($id);
        return view('admin.perjalananDinas.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = PerjalananDinas::findOrFail($id);
        $karyawan = Karyawan::all()->pluck('nama_karyawan', 'id');

        return view('admin.perjalananDinas.edit', compact('item', 'karyawan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, PerjalananDinas::rules(true, $id));

        $item = PerjalananDinas::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.perjalananDinas.index')->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PerjalananDinas::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

