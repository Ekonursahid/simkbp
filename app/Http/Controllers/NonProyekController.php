<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\User;
use App\NonProyek;
use App\NonProyekDetil;
// use App\Notifications\RepliedToNonProyek;

class NonProyekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = NonProyek::latest('updated_at')->get();

        return view('admin.nonProyek.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = null;

        return view('admin.nonProyek.create', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->submitbutton == 'save')
        {
            $this->validate($request, NonProyek::rules());
        
            NonProyek::create($request->all());

            $users = User::where('role', 'HRD')->first();

            // $users->notify(new RepliedToNonProyek($request));

            return back()->withSuccess(trans('app.success_store'));
        }
        else 
        {
            //$item = Karyawan::findOrFail($request->karyawan_id);

            return view('admin.nonProyek.create', compact('item'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = NonProyek::findOrFail($id);
        return view('admin.nonProyek.show', compact('item'));
    }

    public function print($id)
    {
        $item = NonProyek::findOrFail($id);
        return view('admin.nonProyek.print', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = NonProyek::findOrFail($id);

        return view('admin.nonProyek.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, NonProyek::rules(true, $id));

        $item = NonProyek::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.nonProyek.index')->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        NonProyek::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

