<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\User;
use App\Proyek;
use App\ProyekDetil;
use App\BudgetingDetil;
use App\Budgeting;
// use App\Notifications\RepliedToProyek;

class ProyekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Proyek::latest('updated_at')->get();

        return view('admin.proyek.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $item = null;

        $budget = Budgeting::all()->pluck('nama_pekerjaan', 'id');
        $budgeting = Budgeting::all();

        return view('admin.proyek.create', compact('item', 'budget', 'budgeting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->submitbutton == 'save')
        {
            $this->validate($request, Proyek::rules());
        
            Proyek::create($request->all());

            //$users = User::where('role', 'HRD')->first();

            // $users->notify(new RepliedToProyek($request));

            return back()->withSuccess(trans('app.success_store'));
            //return dd($request);
        }
        else 
        {
            if($request->budgeting_detil_id == null)
            {
                return back()->withWarning(trans('data budgeting masih kosong'));
            }
            $item = BudgetingDetil::findOrFail($request->budgeting_detil_id);
            $budgeting = Budgeting::all();
            //return dd($request);
            return view('admin.proyek.create', compact('item', 'budgeting'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $itemProyek = Proyek::findOrFail($id);
        $item = BudgetingDetil::findOrFail($itemProyek->budgeting_detil_id);

        $budgeting = Budgeting::all();
        

        //$idDetil = null;

        return view('admin.proyek.show', compact('item', 'budgeting', 'itemProyek'));
    }

    public function print($id)
    {
        $item = Proyek::findOrFail($id);
        return view('admin.proyek.print', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemProyek = Proyek::findOrFail($id);
        $item = BudgetingDetil::findOrFail($itemProyek->budgeting_detil_id);

        $budgeting = Budgeting::all();

        return view('admin.proyek.edit', compact('itemProyek', 'budgeting', 'item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Proyek::rules(true, $id));

        $item = Proyek::findOrFail($id);

        $item->update($request->all());

        return redirect()->route(ADMIN . '.proyek.index')->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Proyek::destroy($id);

        return back()->withSuccess(trans('app.success_destroy')); 
    }
}

