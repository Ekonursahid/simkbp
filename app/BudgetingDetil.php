<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class BudgetingDetil extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'budgeting_id', 'nama_personil', 'jumlah_bulan', 'honor_satuan', 'nama_kegiatan',
        'nama_unit', 'harga_satuan', 'jumlah', 'total_personil', 'total_non_personil', 'jenis_budget', 'kode_rab'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            // 'karyawan_id' => "required",
            // 'tanggal_izin' => 'required',
            // 'alasan' => 'required',
            // 'diajukan_oleh' => 'required',
            // 'disetujui_oleh' => 'required',
            // 'diketahui_oleh' => 'required',
        ];

        return $commun;
    }

    public function budgeting() 
    {
        return $this->belongsTo('App\Budgeting');
    }
}
