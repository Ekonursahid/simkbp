<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nik', 'nama_karyawan', 'status', 'avatar', 'bio', 'tanggal_masuk',
        'alamat', 'department', 'jabatan'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            'nik'    => "required",
            'nama_karyawan' => 'required',
            'status' => 'required',
            'tanggal_masuk' => 'required'
        ];

        return $commun;
    }

    public function cuti() 
    {
        return $this->hasMany('App\Cuti');
    }

    public function izin() 
    {
        return $this->hasMany('App\Izin');
    }
}
