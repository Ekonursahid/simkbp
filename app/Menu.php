<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_menu', 'nama_menu', 'icon', 'status'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            'kode_menu'    => "required",
            'nama_menu' => 'required',
            'icon' => 'required',
            'status' => 'required',
        ];

        return $commun;
    }
}
