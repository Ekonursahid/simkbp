<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Proyek extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kode_proyek', 'nama_pekerjaan', 'jumlah_dana_ajukan', 'jumlah_dana_disetujui', 
        'untuk_keperluan', 'kategori', 'tanggal_disetujui', 'diajukan_oleh', 'menyetujui',
        'bendahara', 'budgeting_detil_id'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            'nama_pekerjaan' => "required",
            'kode_proyek' => 'required',
            'jumlah_dana_ajukan' => 'required',
            // 'diajukan_oleh' => 'required',
            // 'disetujui_oleh' => 'required',
            // 'diketahui_oleh' => 'required',
        ];

        return $commun;
    }

    public function ProyekDetil() 
    {
        return $this->hasMany('App\ProyekDetil');
    }
}
