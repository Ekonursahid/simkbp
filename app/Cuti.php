<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'karyawan_id', 'keperluan', 'tanggal_cuti', 'tanggal_masuk', 'jumlah_hari_ajuan',
        'status_cuti', 'jumlah_hari_disetujui', 'alasan_tidak_disetujui', 'direksi',
        'hrd'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false)
    {
        $commun = [
            
        ];

        return $commun;
    }

    public function karyawan() 
    {
        return $this->belongsTo('App\Karyawan');
    }
}
