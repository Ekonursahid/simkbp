<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetingDetilsTable extends Migration 
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgeting_detils', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budgeting_id')->unsigned();
            $table->string('kode_rab')->unique();
            $table->string('nama_personil', 100)->nullable();
            $table->boolean('jenis_budget')->nullable();
            $table->integer('jumlah_bulan')->nullable();
            $table->decimal('honor_satuan', 12,2)->nullable();
            $table->decimal('total_personil', 12,2)->nullable();
            $table->string('nama_kegiatan', 50)->nullable();
            $table->string('nama_unit', 50)->nullable();
            $table->decimal('harga_satuan', 12,2)->nullable();
            $table->decimal('total_non_personil', 12,2)->nullable();
            $table->integer('jumlah')->nullable();
            $table->timestamps();

            $table->foreign('budgeting_id')
                ->references('id')
                ->on('budgetings')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgeting_detils');
    }
}
