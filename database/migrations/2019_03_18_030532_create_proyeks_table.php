<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyeks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budgeting_detil_id')->unsigned();
            $table->string('kode_proyek', 10);
            $table->string('nama_pekerjaan', 100);
            $table->decimal('jumlah_dana_ajukan', 12,2);
            $table->decimal('jumlah_dana_disetujui', 12,2)->nullable();
            $table->string('untuk_keperluan', 100);
            $table->string('kategori', 100)->nullable();
            $table->datetime('tanggal_disetujui')->nullable();
            $table->string('diajukan_oleh', 50)->nullable();
            $table->string('menyetujui', 50)->nullable();
            $table->string('bendahara', 50)->nullable();
            $table->boolean('status_proyek')->nullable();
            $table->timestamps();

            $table->foreign('budgeting_detil_id')
                ->references('id')
                ->on('budgeting_detils')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyeks');
    }
}
