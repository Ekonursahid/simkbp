<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUudpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uudps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_pengajuan', 10);
            $table->string('nama', 100);
            $table->string('nama_pekerjaan', 100);
            $table->decimal('jumlah_pengajuan', 12, 2);
            $table->datetime('tanggal_pengajuan')->nullable();
            $table->boolean('status_uudp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uudps');
    }
}
