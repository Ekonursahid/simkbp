<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonProyekDetilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('non_proyek_detils', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('non_proyek_id')->unsigned();
            $table->string('kode_rab', 10);
            $table->string('jenis_pengeluaran', 100);
            $table->decimal('kuantitas', 9,2);
            $table->string('satuan', 50);
            $table->string('harga_satuan', 50);
            $table->decimal('jumlah', 9,2);
            $table->boolean('status_non_proyek')->nullable();
            $table->timestamps();

            $table->foreign('non_proyek_id')
                ->references('id')
                ->on('non_proyeks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('non_proyek_detils');
    }
}
