<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIzinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('karyawan_id')->unsigned();
            $table->datetime('tanggal_izin');
            $table->string('alasan');
            $table->string('diajukan_oleh',100)->nullable();
            $table->string('disetujui_oleh',100)->nullable();
            $table->string('diketahui_oleh',100)->nullable();
            $table->boolean('status_izin')->nullable();
            $table->timestamps();

            $table->foreign('karyawan_id')
                ->references('id')
                ->on('karyawans')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izins');
    }
}
