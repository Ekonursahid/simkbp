<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCutisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cutis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('karyawan_id')->unsigned();
            $table->string('keperluan', 100);
            $table->datetime('tanggal_cuti');
            $table->datetime('tanggal_masuk');
            $table->integer('jumlah_hari_ajuan');
            $table->boolean('status_cuti')->nullable();
            $table->integer('jumlah_hari_disetujui')->nullable();
            $table->string('alasan_tidak_disetujui', 100)->nullable();
            $table->string('direksi', 100)->nullable();
            $table->string('hrd', 100)->nullable();
            $table->timestamps();

            $table->foreign('karyawan_id')
                ->references('id')
                ->on('karyawans')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cutis');
    }
}
