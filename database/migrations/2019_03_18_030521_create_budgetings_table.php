<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgetings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_budget')->unique();
            $table->string('nama_pekerjaan', 100);
            $table->decimal('jumlah_dana_ajukan', 12,2)->nullable();
            $table->decimal('jumlah_dana_disetujui', 12,2)->nullable();
            $table->string('untuk_keperluan', 100)->nullable();
            $table->string('kategori', 100)->nullable();
            $table->datetime('tanggal_disetujui')->nullable();
            $table->string('diajukan_oleh', 50)->nullable();
            $table->string('menyetujui', 50)->nullable();
            $table->string('bendahara', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgetings');
    }
}
