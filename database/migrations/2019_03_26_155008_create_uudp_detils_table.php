<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUudpDetilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uudp_detils', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uudp_id')->unsigned();
            $table->string('kode_rab', 10);
            $table->string('jenis_pengeluaran', 100);
            $table->decimal('kuantitas', 9,2);
            $table->string('satuan', 50);
            $table->string('harga_satuan', 50);
            $table->decimal('jumlah', 9,2);
            $table->boolean('status_uudp')->nullable();
            $table->timestamps();

            $table->foreign('uudp_id')
                ->references('id')
                ->on('uudps')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uudp_detils');
    }
}
