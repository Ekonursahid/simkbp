<?php

use App\Karyawan;
use Illuminate\Database\Seeder;

class karyawans extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        $data = [];
        
        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'nik' => 'T001',
                'alamat' => 'Jl Golf I Block II No 108 Bandung',
                'jabatan' => 'Surveyor/Operator',
                'department' => 'Paket PPL 4 2018',
                'nama_karyawan' => 'M.Rifky Ramadhan',
                'status' => 'Karyawan Tetap',
                'tanggal_masuk' => now(),
            ],
            [
                'nik' => 'T002',
                'alamat' => 'Jl Golf I Block II No 108 Bandung',
                'jabatan' => 'Surveyor/Operator',
                'department' => 'Paket PPL 4 2018',
                'nama_karyawan' => 'Regi Kasogi',
                'status' => 'Karyawan Tetap',
                'tanggal_masuk' => now(),
            ]);
        }
        
        Karyawan::insert($data);
    }
}
