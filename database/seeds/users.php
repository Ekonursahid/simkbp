<?php

use App\User;
use Illuminate\Database\Seeder;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        $data = [];
        
        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'jabatan_id' => '1',
                'name' => 'Eko Nursahid',
                'email' => 'ekonursahid@live.com',
                'password' => bcrypt('ekonursahid'),
                'role'     => 'Administrator',
                'bio'      => $faker->realText(),
            ]);
        }
        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'jabatan_id' => '2',
                'name' => 'Hendra Setiawan',
                'email' => 'hendrasetiawan@gmail.com',
                'password' => bcrypt('hendrasetiawan'),
                'role'     => 'Kepala Bagian HRD',
                'bio'      => $faker->realText(),
            ]);
        }
        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'jabatan_id' => '3',
                'name' => 'Mohammad Ahsan',
                'email' => 'mohammadahsan@gmail.com',
                'password' => bcrypt('mohammadahsan'),
                'role'     => 'Operator HRD',
                'bio'      => $faker->realText(),
            ]);
        }
        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'jabatan_id' => '4',
                'name' => 'Susi Susanti',
                'email' => 'susisusanti@gmail.com',
                'password' => bcrypt('susisusanti'),
                'role'     => 'Operator Keuangan',
                'bio'      => $faker->realText(),
            ]);
        }

        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'jabatan_id' => '5',
                'name' => 'Vita Marissa',
                'email' => 'vitamarissa@gmail.com',
                'password' => bcrypt('vitamarissa'),
                'role'     => 'Manager Keuangan',
                'bio'      => $faker->realText(),
            ]);
        }
        
        User::insert($data);
    }
}
