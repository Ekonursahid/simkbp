<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(jabatans::class);
        $this->call(users::class);
        $this->call(karyawans::class);
    }
}
