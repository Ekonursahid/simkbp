<?php

use App\Jabatan;
use Illuminate\Database\Seeder;

class jabatans extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        $data = [];
        
        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'kode_jabatan' => 'J001',
                'jabatan' => 'Administrator',
                'status' => 1
            ]);
        }
        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'kode_jabatan' => 'J002',
                'jabatan' => 'Kepala Bagian HRD',
                'status' => 1
            ]);
        }
        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'kode_jabatan' => 'J003',
                'jabatan' => 'Operator HRD',
                'status' => 1
            ]);
        }
        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'kode_jabatan' => 'J004',
                'jabatan' => 'Operator Keuangan',
                'status' => 1
            ]);
        }
        for ($i = 1; $i <= 1 ; $i++) {
            array_push($data, [
                'kode_jabatan' => 'J005',
                'jabatan' => 'Manager Keuangan',
                'status' => 1
            ]);
        }
        
        Jabatan::insert($data);
    }
}
